package Zz;

import javax.management.InstanceAlreadyExistsException;
import java.util.HashSet;
import java.util.Random;

public abstract class Person {
    protected static HashSet<String> allIdentifiers = new HashSet<>();
    protected static String[] allNames = LoadNames.loadNames();

    protected String id;
    protected String name;

    public Person() {
        id = generateValidId();
        name = generateRandomName();

        allIdentifiers.add(this.id);
    }

    public Person(String id, String name) throws InstanceAlreadyExistsException {
        if (allIdentifiers.contains(id)) throw new InstanceAlreadyExistsException();

        this.id = id;
        this.name = name;

        allIdentifiers.add(id);
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String generateValidId() {
        String id;

        do {
            id = generateRandomId();
        } while (allIdentifiers.contains(id));

        return id;
    }

    private String generateRandomId() {
        String alphanumerCharacters = "abcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuilder id = new StringBuilder();

        id.append(alphanumerCharacters.charAt(
                random.nextInt(23, alphanumerCharacters.length()))
        );
        for (int i = 0; i < 7; i++) {
            id.append(random.nextInt(10));
        }
        return id.toString() +
                alphanumerCharacters.charAt(random.nextInt(26));
    }

    private String generateRandomName() {
        int randomNameIndex = (int) Math.round(Math.random() * (allNames.length - 1));
        return allNames[randomNameIndex];
    }

    @Override
    public String toString() {
        return "{nombre= " + name +
                ", id= " + id +
                '}';
    }
}



