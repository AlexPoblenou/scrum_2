package Zz;

import java.util.HashMap;
import java.util.HashSet;

public class Storage {
    public static HashSet<String> FOOD_TYPES = new HashSet<>();
    static {
        FOOD_TYPES.add("Pescado");
        FOOD_TYPES.add("Legumbres");
        FOOD_TYPES.add("Agua");
        FOOD_TYPES.add("Carne");
    }

    private int maxCapacity;
    private HashMap<String, Integer> foods;

    public Storage() {
        maxCapacity = -1;
        foods = new HashMap<>();
    }

    public Storage(int maxCapacity){
        this.maxCapacity = maxCapacity;
        this.foods = new HashMap<>();
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }

    public HashMap<String, Integer> getFoods() {
        return foods;
    }

    public void setMaxCapacity(int maxCapacity) {
        this.maxCapacity = maxCapacity;
    }
    
    public void addFood(HashMap<String, Integer> donation) {
        foods.replaceAll(
                (foodType, amount) -> amount += (donation.get(foodType) == null) ? 0 : donation.get(foodType)
        );
    }

    public HashMap<String, Integer> extract() {
        HashMap<String, Integer> out = (HashMap<String, Integer>) foods.clone();
        foods.clear();
        return out;
    }

    public void start() {
        FOOD_TYPES.forEach(foodType -> foods.put(foodType, 0));
    }

    public void end() {
        foods.clear();
    }

    @Override
    public String toString() {
        return "{maxCapacity= " + maxCapacity +
                ", foods= " + foods.toString()
                + '}';
    }
}
