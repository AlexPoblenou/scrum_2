package Zz;

import java.util.ArrayList;
import java.util.HashMap;

public class Warehouse {
    public static final int WAREHOUSE_VOLUNTEERS = 3;

    protected Storage storage;
    protected ArrayList<Volunteer> volunteers;

    public Warehouse() {
        storage = new Storage();
        volunteers = new ArrayList<>();
    }

    public Warehouse(int maxStorageCapacity) {
        storage = new Storage(maxStorageCapacity);
        volunteers = new ArrayList<>();
    }

    public Storage getStorage() {
        return storage;
    }

    public void setStorage (Storage storage) {
        this.storage = storage;
    }

    public final boolean isFull() {
        return volunteers.size() == WAREHOUSE_VOLUNTEERS;
    }

    public void addVolunteers(Volunteer... volunteers) {
        for (Volunteer volunteer : volunteers) {
            this.volunteers.add(volunteer);
        }
    }

    public void start() {
        storage.start();
    }

    public void end() {
        storage.end();
    }

    public void store(HashMap<String, Integer> food) {
        storage.addFood(food);
    }

    @Override
    public String toString() {
        return "Almacen{" +
                "depositoAlmacen=" + storage +
                '}';
    }
}
