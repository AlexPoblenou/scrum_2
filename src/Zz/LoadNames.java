package Zz;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

public class LoadNames {
    public static String[] loadNames() {
        LinkedList<String> listOfNames = new LinkedList<>();

        try(BufferedReader br = new BufferedReader(new FileReader("nombres"))) {
            String line;
            while ((line = br.readLine()) != null) {
                listOfNames.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return listOfNames.toArray(new String[0]);
    }
}