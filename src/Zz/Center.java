package Zz;

public class Center extends Warehouse implements nonCentral {
    private final String name;
    private Volunteer carrier;

    public Center(String name) {
        this.name = name;
    }

    public Center(String name, int maxStorageCapacity) {
        super(maxStorageCapacity);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Storage getStorage() {
        return storage;
    }

    public void setStorage(Storage storage) {
        this.storage = storage;
    }

    public boolean hasCarrier() {
        return this.carrier != null;
    }

    public Volunteer getCarrier() {
        return carrier;
    }

    public void setCarrier(Volunteer carrier) {
        this.carrier = carrier;
    }

    @Override
    public boolean isFilled() {
        return storage.getFoods().values().stream().mapToInt(n -> n).sum() >= getStorage().getMaxCapacity();
    }

    @Override
    public void transferFoodTo(Warehouse warehouse) {
        warehouse.store(this.storage.extract());
    }

    @Override
    public String toString() {
        return "Centro{" +
                "nombre= " + name +
                ", voluntarios= " + volunteers.size() +
                ", transportista= " + carrier +
                ", deposito= " + storage +
                '}';
    }
}
