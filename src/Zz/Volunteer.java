package Zz;

public class Volunteer extends Person {

    public Volunteer() {
        super();
    }

    public static Volunteer[] getRandomVolunteers(int amount) {
        Volunteer[] volunteers = new Volunteer[amount];
        for (int i = 0; i < amount; i++) {
            volunteers[i] = new Volunteer();
        }
        return volunteers;
    }
}
