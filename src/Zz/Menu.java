package Zz;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;
import databaseConnection.CenterDatabase;
import databaseConnection.VolunteerDatabase;
import models.Center;
import models.Volunteer;


public final class Menu {
    Scanner sc;
    static CenterDatabase centerDatabase = new CenterDatabase();
    static VolunteerDatabase volunteerDatabase = new VolunteerDatabase();
    Warehouse warehouseCentral;
    HashSet<Center> centers;
    
    public Menu() {
        sc = new Scanner(System.in);
        warehouseCentral = new Warehouse();
        centers = new HashSet<>();
    }

    private void getCenter(int centerId) {
        /*return centers.stream().filter(
                center -> center.getName().equals(name)
        ).findFirst().orElse(null);*/

        System.out.println("Introduce la id del centro");
        try {
            System.out.println(centerDatabase.readOne(centerId));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void mainMenu() {

        int option;
        do {
            try {
                System.out.println("----------MENU----------\n");
                System.out.println("Selecciona una opcion:\n");
                System.out.println("1. Añadir centro.");
                System.out.println("2. Eliminar centro.");
                System.out.println("3. Listar los centros.");
                System.out.println("4. Añadir voluntarios.");
                System.out.println("5. Empezar recolecta.");
                System.out.println("0. Salir.");

                option = sc.nextInt();
                sc.nextLine();

                if (centerDatabase.readAll() == null && (option > 1 && option <= 5)) {
                    System.out.println("Tiene que existir un centro antes de configurarlo.\n");
                } else switch (option) {
                    case 0 -> {}
                    case 1 -> addCenter();
                    case 2 -> deleteCenter();
                    case 3 -> listCenter();
                    case 4 -> addVolunteers();
                    //case 5 -> startCollection();
                    default -> System.out.println("Esa opcion no existe. Elije otra.\n");
                }
            } catch (InputMismatchException e) {
                System.out.println("Tienes que introducir un valor entero: " + e.getMessage());
                option = -1;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        } while (option != 0);
    }

    private void addCenter() throws SQLException {
        System.out.println("Nombre del centro: ");
        String centerName = sc.nextLine();
        System.out.println("Capacidad maxíma de deposito: ");
        int maxStorageCapacity = sc.nextInt();
        Center center = new Center(0,centerName, maxStorageCapacity);
        centers.add(center);
        centerDatabase.create(center);
        System.out.println("Centro " + centerName + " añadido!");
    }

    private void deleteCenter() throws SQLException {
        /*System.out.println("Indica el nombre del centro que quieres eliminar:");
        String nameCenter = sc.nextLine();
        boolean centerFound = false;
        for (Center c : centers) {
            if (c.getName().equals(nameCenter)) {
                centers.remove(c);
                centerFound = true;
                break;
            }
        }
        if (centerFound) {
            System.out.println("El centro " + nameCenter + " ha sido eliminado.");
        } else {
            System.out.println("El centro " + nameCenter + " no existe.");
        }*/

        System.out.println("Indica el id del centro que quieres eliminar:");
        int idCenter = sc.nextInt();
        centerDatabase.delete(idCenter);

    }

    private void listCenter() {
        /*System.out.println("Hay " + centers.size() + " centros:");
        centers.forEach(System.out::println);*/
        try {
            System.out.println(centerDatabase.readAll());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }



    private void addVolunteers() {
        System.out.println("Nombre del voluntario");
        String volunteerName = sc.nextLine();
        System.out.println("ID del centro a asignar");
        int centerId = sc.nextInt();
        volunteerDatabase.create(volunteerName, centerId);
        System.out.println("Se ha añadido el voluntario " + volunteerName + " al centro " + centerId);
    }

    private void readVolunteer(){

    }

    private void readAllByCenterId(){

        System.out.println("Introduce el id del centro");
        int centerId = sc.nextInt();
        try {
            volunteerDatabase.readByCenter(centerId);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void deleteVolunteer(){

        System.out.println("Introduce el id del voluntario a eliminar");
        int volunteerId = sc.nextInt();
        try {
            volunteerDatabase.delete(volunteerId);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Voluntario " + volunteerId + " eliminado con exito.");

    }

    private void readAllVolunteers(){
        try {
            System.out.println(volunteerDatabase.readAll());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }




    /*private void distributeVolunteers(int amount) {
        while (!warehouseCentral.isFull() && amount > 0) {
            warehouseCentral.addVolunteers(new Volunteer());
            amount--;
        }

        int volunteersCenter = amount / centers.size();
        final int[] leftoverVolunteers = {amount % centers.size()};
        centers.stream().sorted(
                (c1, c2) -> {
                    int s1 = c1.getStorage().getMaxCapacity();
                    int s2 = c2.getStorage().getMaxCapacity();
                    return -Integer.compare(s1, s2);
                }
        ).forEach(
                center -> {
                    center.addVolunteers(Volunteer.getRandomVolunteers(volunteersCenter));
                    if (leftoverVolunteers[0] > 0) {
                        center.addVolunteers(new Volunteer());
                        leftoverVolunteers[0] -= 1;
                    }
                }
        );
    }

    */

    private void startCollection() throws SQLException {
        warehouseCentral.start();
        centers.forEach(Warehouse::start);

        int option;
        do {
            System.out.println("1. Añadir comida.");
            System.out.println("0. Salir.");
            option = sc.nextInt();
            sc.nextLine();
            switch (option) {
                case 0 -> {}
                case 1 -> addFood();
                default -> System.out.println("Elige una opcion valida");
            }
        } while (option != 0);

        centers.forEach(center -> center.transferFoodTo(warehouseCentral));

        System.out.println("Comida total recolectada:");
        System.out.println(warehouseCentral.getStorage().getFoods());

        warehouseCentral.end();
        centers.forEach(Warehouse::end);
    }

    private void addFood() throws SQLException {
        System.out.println("A que centro quieres donar?");
        int centerId = sc.nextInt();

        Center center = centerDatabase.readOne(centerId);

        while (center == null) {
            System.out.println("Este centro no existe");
            System.out.println("A que centro quieres donar?");
            center = centerDatabase.readOne(sc.nextInt());
        }

        System.out.print("Que producto quieres añadir?\nTipos: ");
        System.out.println(Storage.FOOD_TYPES.stream().collect(Collectors.joining(", ")));

        String type = sc.nextLine();
        while (!Storage.FOOD_TYPES.contains(type)) {
            System.out.println("Este producto no existe o no esta contemplado.");
            System.out.print("Que producto quieres añadir?\nTipos: ");
            System.out.println(Storage.FOOD_TYPES.stream().collect(Collectors.joining(", ")));
            type = sc.nextLine();
        }

        System.out.println("Cuanta cantidad?");
        int amount = sc.nextInt();

        HashMap<String, Integer> donation = new HashMap<>();
        donation.put(type, amount);
        center.store(donation);
        if (center.isFilled()) center.transferFoodTo(warehouseCentral);
    }




}
