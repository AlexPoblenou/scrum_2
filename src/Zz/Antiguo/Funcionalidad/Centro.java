package Zz.Antiguo.Funcionalidad;

public class Centro {
    private String nombre;

    private int capacidad;
    private int[] cantidadAlimento;
    private int depositosLlenos, depositoActual;

    private int voluntarios;

    public Centro(String nombre){
        this.nombre = nombre;
        resetCentro();
    }

    public void resetCentro() {
        this.cantidadAlimento = new int[5];
        this.depositosLlenos = 0;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public void setVoluntarios(int voluntarios) {
        this.voluntarios = voluntarios;
    }

    public void addVoluntarios(int voluntarios) {
        this.voluntarios += voluntarios;
    }

    public void addAlimento(int tipo, int cantidad) {
        cantidadAlimento[tipo] += cantidad;
        depositoActual += cantidad;
        while (depositoActual >= capacidad) {
            depositosLlenos++;
            depositoActual -= capacidad;
        }
    }

    public int getCapacidad() {
        return this.capacidad;
    }

    public int getDepositosLlenos() {
        return depositosLlenos;
    }

    public int getVoluntarios() {
        return this.voluntarios;
    }

    public int getCapacidadActual() {
        return this.depositoActual;
    }

    public int[] getCantidadAlimento() {
        return this.cantidadAlimento;
    }
}
