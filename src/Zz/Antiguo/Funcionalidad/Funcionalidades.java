package Zz.Antiguo.Funcionalidad;
import java.util.Arrays;
import java.util.HashMap;

public class Funcionalidades {
    private static HashMap<String, Centro> centros = new HashMap<>();
    private static final String[] tiposAlimento = {"Carne", "Pescado", "Verdura", "Cereales", "Legumbres", "Bebida"};

    public static void añadirCentro(String nombre, int capacidad, int voluntarios) {
        centros.put(nombre, new Centro(nombre));
        centros.get(nombre).setCapacidad(capacidad);
        centros.get(nombre).setVoluntarios(voluntarios);
    }

    public static void eliminarCentro(String nombre) {
        centros.remove(nombre);
    }

    public static boolean hayCentros() {
        return !centros.isEmpty();
    }

    public static boolean existeCentro(String nombre) {
        return centros.containsKey(nombre);
    }

    public static void añadirVoluntariosCentro(String centro, int voluntarios) {
        centros.get(centro).addVoluntarios(voluntarios);
    }

    public static void repartirVoluntarios(int voluntarios) {
        String[] nombresCentro = centros.keySet().toArray(new String[0]);

        for (String nombre : nombresCentro) {
            añadirVoluntariosCentro(nombre, voluntarios / nombresCentro.length);
        }

        voluntarios = voluntarios % nombresCentro.length;
        while (voluntarios > 0) {
            añadirVoluntariosCentro(nombresCentro[voluntarios], 1);
            voluntarios--;
        }
    }

    public static String getTiposAlimento() {
        return Arrays.toString(tiposAlimento);
    }

    public static boolean existeAlimento(String alimento) {
        for (String tipo : tiposAlimento) {
            if (alimento.equals(tipo)) return true;
        }
        return false;
    }

    public static int getVoluntarios(String nombre) {
        return centros.get(nombre).getVoluntarios();
    }

    public static int getCapacidad(String nombre) {
        return centros.get(nombre).getCapacidad();
    }

    public static int getCapacidadActual(String nombre) {
        return centros.get(nombre).getCapacidadActual();
    }

    public static int getDepositosLlenados(String nombre) {
        return centros.get(nombre).getDepositosLlenos();
    }

    public static int getDepositoActual(String nombre) {
        return centros.get(nombre).getCapacidadActual();
    }

    public static void addAlimento(String nombre, String alimento, int cantidad) {
        int tipoAlimento = 0;
        for (int i = 0; i < tiposAlimento.length; i++) {
            if (tiposAlimento[i].equals(alimento)) {
                tipoAlimento = i;
                break;
            }
        }
        centros.get(nombre).addAlimento(tipoAlimento, cantidad);
    }

    public static int[] getAlimentosCentro(String nombre) {
        return centros.get(nombre).getCantidadAlimento();
    }
}
