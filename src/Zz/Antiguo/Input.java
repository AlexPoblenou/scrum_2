package Zz.Antiguo;

import java.util.Scanner;

public class Input {

    static Scanner sc = new Scanner(System.in);

    public static String getLine(){
        return sc.nextLine();
    }

    public static int getNum() {
        String input;
        do {
            input = getLine();
        } while (!isNum(input));
        return Integer.parseInt(input);
    }

    public static boolean isNum(String input) {
        for (int i = 0; i < input.length(); i++) {
            if (!Character.isDigit(input.charAt(i))) return false;
        }
        return true;
    }

    public static int getNum(int max) {
        int input;
        do {
            input = getNum();
        } while (input < 0 || input > max);
        return input;
    }

    public static int getNumBetween(int min, int max) {
        int input;
        do {
            input = getNum();
        } while (input < min || input > max);
        return input;

    }

}
