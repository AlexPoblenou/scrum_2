package Zz.Antiguo;

import Zz.Antiguo.Funcionalidad.Funcionalidades;

import java.util.Arrays;

public class Menu {

    public static void menuPrincipal() {

        int option;
        do {
            System.out.println("Escoje una opcion:\n");
            System.out.println("1. Añadir centro.");
            System.out.println("2. Añadir voluntarios.");
            System.out.println("3. Añadir comida.");
            System.out.println("4. Estado centros.");
            System.out.println("5. Retirar centro.");
            System.out.println("0. Salir.");

            option = Input.getNum();
            if (!Funcionalidades.hayCentros() && (option == 2 || option == 3 || option == 4 || option == 5)) {
                System.out.println("Tiene que existir un centro antes de configurarlo.\n");
            } else switch (option) {
                case 1:
                    añadirCentro();
                    break;
                case 2:
                    añadirVoluntarios();
                    break;
                case 3:
                    añadirAlimento();
                    break;
                case 4:
                    estadoCentro();
                    break;
                case 5:
                    retirarCentro();
                    break;
                case 0: break;
                default:
                    System.out.println("Esa opcion no existe. Elije otra.\n");
                    break;
            }
        } while (option != 0);
    }

    public static void añadirCentro() {
        System.out.println("Escribe nombre del centro: ");
        String nombre = Input.getLine();
        while (Funcionalidades.existeCentro(nombre)) {
            System.out.println("Este centro ya existe.");
            System.out.println("Pon un centro valido:\n");
            nombre = Input.getLine();
        }
        System.out.println("Añade capacidad del centro: ");
        int capacidad = Input.getNum();
        System.out.println("Añade voluntarios del centro: ");
        int voluntarios = Input.getNum();

        Funcionalidades.añadirCentro(nombre, capacidad, voluntarios);
    }

    public static void añadirVoluntarios() {
        System.out.println("Selecciona una opcion: ");
        System.out.println("1. Añadir voluntarios totales: ");
        System.out.println("2. Añadir voluntarios a un centro en concreto: ");
        switch (Input.getNumBetween(1, 2)) {
            case 1:
                System.out.println("cuantos voluntarios quieres añadir? ");
                Funcionalidades.repartirVoluntarios(Input.getNum());
                break;
            case 2:
                System.out.println("Indica nombre del centro: ");
                String nombre = Input.getLine();
                while (!Funcionalidades.existeCentro(nombre)) {
                    System.out.println("Este centro no existe.");
                    System.out.println("Pon un centro valido:\n");
                    nombre = Input.getLine();
                }

                System.out.println("Cuantos voluntarios quieres añadir? ");
                Funcionalidades.añadirVoluntariosCentro(nombre, Input.getNum());
                break;
        }
    }

    public static void añadirAlimento() {
        System.out.println("Escoge el centro: ");
        String nombre = Input.getLine();
        while (!Funcionalidades.existeCentro(nombre)) {
            System.out.println("Este centro no existe.");
            System.out.println("Pon un centro valido:\n");
            nombre = Input.getLine();
        }

        System.out.println("Existen estos tipos de alimentos: ");
        System.out.println(Funcionalidades.getTiposAlimento());
        System.out.println("Escribe el nombre del alimento que quieras añadir: ");
        String alimento = Input.getLine();
        while (!Funcionalidades.existeAlimento(alimento)) {
            System.out.println("Este alimento no existe.");
            System.out.println("Pon un alimento valido:\n");
            alimento = Input.getLine();
        }

        System.out.println("Cuantos quieres añadir: ");
        Funcionalidades.addAlimento(nombre, alimento, Input.getNum());
    }

    public static void estadoCentro() {
        System.out.println("Escoge el centro: ");
        String nombre = Input.getLine();
        while (!Funcionalidades.existeCentro(nombre)) {
            System.out.println("Este centro no existe.");
            System.out.println("Pon un centro valido:\n");
            nombre = Input.getLine();
        }
        System.out.println("--------------------\n");
        System.out.println("Nombre del centro: " + nombre);
        System.out.println("Total voluntarios: " + Funcionalidades.getVoluntarios(nombre));
        System.out.println("Depositos llenados: ");
        System.out.println(Funcionalidades.getDepositosLlenados(nombre));
        System.out.println("Capacidad deposito total: ");
        System.out.println(Funcionalidades.getCapacidad(nombre));
        System.out.println("Capacidad deposito usado: ");
        System.out.println(Funcionalidades.getDepositoActual(nombre));
        System.out.println("Alimentos: ");
        System.out.println(Arrays.toString(Funcionalidades.getAlimentosCentro(nombre)));
        System.out.println("\n--------------------");
    }

    public static void retirarCentro() {
        System.out.println("Escoge el centro que retirar: ");
        String nombre = Input.getLine();
        while (!Funcionalidades.existeCentro(nombre)) {
            System.out.println("Este centro no existe.");
            System.out.println("Pon un centro valido:\n");
            nombre = Input.getLine();
        }
        Funcionalidades.eliminarCentro(nombre);
        System.out.println(nombre+ " ha sido eliminado.");
    }
}



