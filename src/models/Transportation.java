package models;

public record Transportation(int transportationId, int centerId, String time) {
}
