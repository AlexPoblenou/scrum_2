package models;

public record Center(int centerId, String name, int capacity) {
    public Center(String name, int capacity) {
        this(-1, name, capacity);
    }
}
