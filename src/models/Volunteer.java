package models;

public record Volunteer(int volunteerId, String name, int centerId) {
    public Volunteer(int centerId) {
        this(-1, Names.getRandomName(), centerId);
    }
}