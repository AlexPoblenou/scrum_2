package models;

public record Donator(int donatorId, String name) {
}
