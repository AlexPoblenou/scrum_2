package models;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

public class Names {
    private static String[] names = loadNames();

    public static String getRandomName() {
        return names[(int) (Math.random() * names.length)];
    }

    private static String[] loadNames() {
        LinkedList<String> listOfNames = new LinkedList<>();
        String line;

        try (BufferedReader br = new BufferedReader(new FileReader("nombres"))) {
            while ((line = br.readLine()) != null) {
                listOfNames.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return listOfNames.toArray(new String[0]);
    }
}
