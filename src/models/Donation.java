package models;

import java.util.HashMap;

public record Donation(int donationId, int donatorId, int centerId, HashMap<String, Integer> donation, String time) {
    public Donation(int donatorId, int centerId, HashMap<String, Integer> donation, String time) {
        this(-1, donatorId, centerId, donation, time);
    }
}
