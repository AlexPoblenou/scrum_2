package databaseConnection;

import daoInterfaces.VolunteerDAO;
import models.Volunteer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class VolunteerDatabase implements VolunteerDAO {
    Connection con;

    public VolunteerDatabase() {
        this.con = Connexio.getConnection();
    }

    @Override
    public int create(Volunteer volunteer) throws SQLException {

        String query = "insert into Voluntarios(nombre, centroId) values (?, ?)";
        PreparedStatement ps = con.prepareStatement(query);

        ps.setString(1, volunteer.name());
        ps.setInt(2, volunteer.centerId());
        ps.executeUpdate();

        ResultSet rs = ps.getGeneratedKeys();
        rs.next();
        int n = rs.getInt(1);
        return n;
    }

    @Override
    public Volunteer readOne(int id) throws SQLException {

        String query = "select * from Voluntarios where voluntarioId= ?";
        PreparedStatement ps = con.prepareStatement(query);

        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();

        if (rs.next()) {

            int volunteerId = rs.getInt("voluntarioId");
            String name = rs.getString("nombre");
            int centerId = rs.getInt("centroId");

            return new Volunteer(volunteerId, name, centerId);
        } else return null;
    }

    @Override
    public void updateCenter(int id, int centerId) throws SQLException {
        String query = "update Voluntarios set centroId = (?) where voluntarioId = ?";
        PreparedStatement ps = con.prepareStatement(query);

        ps.setInt(1, centerId);
        ps.setInt(2, id);
        ps.executeUpdate();
    }

    @Override
    public void delete(int id) throws SQLException {
        String query = "delete from Voluntarios where voluntarioId =?";
        PreparedStatement ps = con.prepareStatement(query);
        ps.setInt(1, id);
        ps.executeUpdate();
    }

    @Override
    public List<Volunteer> readAll() throws SQLException {
        String query = "select * from Voluntarios";
        PreparedStatement ps = con.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        List<Volunteer> ls = new ArrayList();

        while (rs.next()) {

            int volunteerId = rs.getInt("voluntarioId");
            String name = rs.getString("nombre");
            int centerId = rs.getInt("centroId");

            Volunteer volunteer = new Volunteer(volunteerId, name, centerId);

            ls.add(volunteer);
        }
        return ls;
    }

    @Override
    public List<Volunteer> readByCenter(int centerId) throws SQLException {
        String query = "select * from Voluntarios where centroId = ?";
        PreparedStatement ps = con.prepareStatement(query);
        ps.setInt(1, centerId);
        ResultSet rs = ps.executeQuery();
        List<Volunteer> ls = new ArrayList();

        while (rs.next()) {

            int volunteerId = rs.getInt("voluntarioId");
            String name = rs.getString("nombre");
            int idCentro = rs.getInt("centroId");

            Volunteer volunteer = new Volunteer(volunteerId, name, idCentro);

            ls.add(volunteer);
        }
        return ls;
    }
}
