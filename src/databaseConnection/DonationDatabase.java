package databaseConnection;

import daoInterfaces.DonationDAO;
import models.Donation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DonationDatabase implements DonationDAO {

    Connection con;

    public DonationDatabase() {
        this.con = Connexio.getConnection();
    }

    @Override
    public int create(Donation donation) throws SQLException {

        String query = "insert into Donacion(donanteid, centroId, carne, pescado, agua, legumbres, verdura, hora) values (?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement ps = con.prepareStatement(query);

        ps.setInt(1, donation.donatorId());
        ps.setInt(2, donation.centerId());
        ps.setInt(3, donation.donation().get("carne"));
        ps.setInt(4, donation.donation().get("pescado"));
        ps.setInt(5, donation.donation().get("agua"));
        ps.setInt(6, donation.donation().get("legumbres"));
        ps.setInt(7, donation.donation().get("verdura"));
        ps.setString(8, donation.time());

        ps.executeUpdate();

        ResultSet rs = ps.getGeneratedKeys();
        rs.next();
        return rs.getInt(1);
    }

    @Override
    public Donation readOne(int id) throws SQLException {

        String query = "select * from Donacion where donacionId = ?";
        PreparedStatement ps = con.prepareStatement(query);

        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();

        if (rs.next()) {

            int donationId = rs.getInt("donacionId");
            int donatorId = rs.getInt("donanteId");
            int centerId = rs.getInt("centroId");
            int carne = rs.getInt("carne");
            int pescado = rs.getInt("pescado");
            int agua = rs.getInt("agua");
            int legumbres = rs.getInt("legumbres");
            int verdura = rs.getInt("verdura");
            String time = rs.getString("hora");

            HashMap<String, Integer> donation = new HashMap<>();
            donation.put("carne", carne);
            donation.put("pescado", pescado);
            donation.put("agua", agua);
            donation.put("legumbres", legumbres);
            donation.put("verdura", verdura);

            return new Donation(donationId, donatorId, centerId, donation, time);
        } else return null;
    }

    @Override
    public List<Donation> readAll() throws SQLException {
        String query = "select * from Centros";
        PreparedStatement ps = con.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        List<Donation> ls = new ArrayList<>();

        while (rs.next()) {

            int donationId = rs.getInt("donacionId");
            int donatorId = rs.getInt("donanteId");
            int centerId = rs.getInt("centroId");
            int carne = rs.getInt("carne");
            int pescado = rs.getInt("pescado");
            int agua = rs.getInt("agua");
            int legumbres = rs.getInt("legumbres");
            int verdura = rs.getInt("verdura");
            String time = rs.getString("hora");

            HashMap<String, Integer> donation = new HashMap<>();
            donation.put("carne", carne);
            donation.put("pescado", pescado);
            donation.put("agua", agua);
            donation.put("legumbres", legumbres);
            donation.put("verdura", verdura);

            ls.add(new Donation(donationId, donatorId, centerId, donation, time));
        }
        return ls;
    }
}
