package databaseConnection;

import daoInterfaces.DonatorDAO;
import models.Donator;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DonatorDatabase implements DonatorDAO {

    Connection con;

    public DonatorDatabase() {
        this.con = Connexio.getConnection();
    }

    @Override
    public int create(Donator donator) throws SQLException {

        String query = "insert into Donantes(nombre) values (?)";
        PreparedStatement ps = con.prepareStatement(query);

        ps.setString(1, donator.name());
        ps.executeUpdate();

        ResultSet rs = ps.getGeneratedKeys();
        rs.next();
        int n = rs.getInt(1);
        return n;
    }

    @Override
    public Donator readOne(int id) throws SQLException {

        String query = "select * from Donantes where donanteId= ?";
        PreparedStatement ps = con.prepareStatement(query);

        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();

        if (rs.next()) {

            int donatorId = rs.getInt("donanteId");
            String name = rs.getString("nombre");

            return new Donator(donatorId, name);
        } else return null;
    }

    @Override
    public List<Donator> readAll() throws SQLException {
        String query = "select * from Donantes";
        PreparedStatement ps = con.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        List<Donator> ls = new ArrayList();

        while (rs.next()) {

            int donatorId = rs.getInt("donanteId");
            String name = rs.getString("nombre");

            Donator donator = new Donator(donatorId, name);

            ls.add(donator);
        }
        return ls;
    }
    @Override
    public boolean exists(int id) throws SQLException {
        String query = "select * from Donantes where donanteId= ?";
        PreparedStatement ps = con.prepareStatement(query);

        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();

        return rs.next();
    }
}
