package databaseConnection;

import daoInterfaces.TransportationDAO;
import models.Transportation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class TransportationDatabase implements TransportationDAO {
    Connection con;

    public TransportationDatabase() {
        this.con = Connexio.getConnection();
    }

    @Override
    public int create(Transportation transportation) throws SQLException {
        String query = "insert into Transporte(centroId, hora) values (?, ?)";
        PreparedStatement ps = con.prepareStatement(query);

        ps.setInt(1, transportation.centerId());
        ps.setString(2, transportation.time());
        ps.executeUpdate();

        ResultSet rs = ps.getGeneratedKeys();
        rs.next();
        int n = rs.getInt(1);
        return n;
    }

    @Override
    public Transportation readLatest(int targetCenterId) throws SQLException {
        String query = "select * from Transporte where centroId = ? ORDER BY hora DESC";
        PreparedStatement ps = con.prepareStatement(query);

        ResultSet rs = ps.executeQuery();

        if (rs.next()) {

            int transportId = rs.getInt("transporteId");
            int centerId = rs.getInt("centroId");
            String time = rs.getString("hora");

            return new Transportation(transportId, centerId, time);
        } else return null;
    }

    @Override
    public List<Transportation> readAllFrom(String time) throws SQLException {
        String query = "select * from Transporte where hora < ?";
        PreparedStatement ps = con.prepareStatement(query);

        ps.setString(1, time);

        ResultSet rs = ps.executeQuery();
        List<Transportation> transportations = new LinkedList<>();

        while (rs.next()) {

            int transportId = rs.getInt("transporteId");
            int centerId = rs.getInt("centroId");
            String hora = rs.getString("hora");

            transportations.add(new Transportation(transportId, centerId, hora));
        }

        return transportations;
    }
}
