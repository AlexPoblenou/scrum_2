package databaseConnection;

import daoInterfaces.CenterDAO;
import models.Center;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CenterDatabase implements CenterDAO {

    Connection con;

    public CenterDatabase() {
        this.con = Connexio.getConnection();
    }

    @Override
    public int create(Center center) throws SQLException {

        String query = "insert into Center(nombre, capacidad) values (?, ?)";
        PreparedStatement ps = con.prepareStatement(query);

        ps.setString(1, center.name());
        ps.setInt(2, center.capacity());
        ps.executeUpdate();

        ResultSet rs = ps.getGeneratedKeys();
        rs.next();
        int n = rs.getInt(1);
        return n;
    }

    @Override
    public Center readOne(int id) throws SQLException {

        String query = "select * from Centros where centroId= ?";
        PreparedStatement ps = con.prepareStatement(query);

        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();

        if (rs.next()) {

            int centerId = rs.getInt("centroId");
            String name = rs.getString("nombre");
            int capacity = rs.getInt("capacidad");

            return new Center(centerId, name, capacity);
        } else return null;
    }

    @Override
    public void update(int id, Center center) throws SQLException {
        String query = "update Centros set nombre, capacidad = (?, ?) where centroId = ?";
        PreparedStatement ps = con.prepareStatement(query);

        ps.setString(1, center.name());
        ps.setInt(2, center.capacity());
        ps.setInt(3, id);
        ps.executeUpdate();
    }

    @Override
    public void delete(int id) throws SQLException {
        String query = "delete from Centros where centroId = ?";
        PreparedStatement ps = con.prepareStatement(query);
        ps.setInt(1, id);
        ps.executeUpdate();
    }

    @Override
    public List<Center> readAll() throws SQLException {
        String query = "select * from Centros";
        PreparedStatement ps = con.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        List<Center> ls = new ArrayList();

        while (rs.next()) {

            int centerId = rs.getInt("centroId");
            String name = rs.getString("nombre");
            int capacity = rs.getInt("capacidad");

            Center center = new Center(centerId, name, capacity);

            ls.add(center);
        }
        return ls;
    }

    @Override
    public boolean exists(int id) throws SQLException {
        String query = "select * from Centros where centroId= ?";
        PreparedStatement ps = con.prepareStatement(query);

        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();

        return rs.next();
    }
}
