package app;

import databaseConnection.CenterDatabase;
import databaseConnection.DonationDatabase;
import databaseConnection.DonatorDatabase;
import databaseConnection.VolunteerDatabase;
import models.Center;
import models.Donation;
import models.Volunteer;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public class BusinessLogic {
    private CenterDatabase centersDB;
    private VolunteerDatabase volunteersDB;
    private DonationDatabase donationsDB;

    public BusinessLogic() {
        centersDB = new CenterDatabase();
        volunteersDB = new VolunteerDatabase();
        donationsDB = new DonationDatabase();
    }

    public void displayOneCenter(int centerId) throws SQLException {
        Center center = centersDB.readOne(centerId);

        System.out.println(center);
    }

    public void displayAllCenters() throws SQLException {
        List<Center> centers = centersDB.readAll();

        centers.forEach(System.out::println);
    }

    public void createCenter(String name, int maxCapacity) throws SQLException {
        Center center = new Center(name, maxCapacity);

        centersDB.create(center);
    }

    public boolean deleteCenter(int centerId) throws SQLException {
        boolean found = centersDB.exists(centerId);
        centersDB.delete(centerId);
        return found;
    }

    public void addVolunteers(int amount, int centerId) throws SQLException {
        for (int i = 0; i < amount; i++) {
            Volunteer volunteer = new Volunteer(centerId);

            volunteersDB.create(volunteer);
        }
    }

    public void distributeVolunteers(int amount) throws SQLException {
        List<Center> centers = centersDB.readAll();
        centers.stream()
                .mapToInt(Center::centerId)
                .forEach(centerId -> {
                    try {
                        this.addVolunteers(amount, centerId);
                    } catch (SQLException e) {
                        System.err.println("Error al introducir voluntarios");
                    }
                });
    }

    public void readByCenterId(int centerId) throws SQLException {
        List<Volunteer> volunteers = volunteersDB.readByCenter(centerId);

        volunteers.forEach(System.out::println);
    }

    public void createDonation(int centerId, int donatorId, int carne, int pescado, int agua, int legumbres, int verdura, String hora) throws SQLException {
        HashMap<String, Integer> donation = new HashMap<>();

        donation.put("carne", carne);
        donation.put("pescado", pescado);
        donation.put("agua", agua);
        donation.put("legumbres", legumbres);
        donation.put("verdura", verdura);

        donationsDB.create(new Donation(centerId, donatorId, donation, hora));
    }
}
