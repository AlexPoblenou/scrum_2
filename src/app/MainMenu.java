package app;

import java.sql.SQLException;
import java.util.*;


public class MainMenu {
    private Scanner sc;
    private BusinessLogic app;

    public MainMenu() {
        sc = new Scanner(System.in);
        app = new BusinessLogic();
    }

    public void mainMenu() {
        int option;
        do {
            try {
                System.out.println("----------MENU----------\n" +
                        "Selecciona una opcion:\n" +
                        "1. Centros.\n" + //hecho
                        "2. Voluntarios.\n" + //hecho
                        "3. Empezar dia.\n" +
                        "4.Estadisticas\n" +//hecho
                        "0. Salir.");


                option = sc.nextInt();
                sc.nextLine();

                switch (option) {
                    case 0 -> {
                    }
                    case 1 -> centerMenu();
                    case 2 -> volunteerMenu();
                    case 3 -> collectionMenu();
                    case 4 -> statistics();
                    default -> System.out.println("Esa opcion no existe. Elije otra.\n");
                }
            } catch (InputMismatchException e) {
                System.out.println("Tienes que introducir un valor entero: " + e.getMessage());
                option = -1;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        } while (option != 0);
    }


    /*---------------------------FUNCIONES DE CENTRO-----------------------*/
    public void centerMenu() {
        System.out.println("1. Añadir centro\n" +
                "2. Ver un centro\n" +
                "3. Ver todos los centros\n" +
                "4. Eliminar centro\n" +
                "0. Salir\n");

        int opcion = sc.nextInt();
        while (opcion != 0) {
            switch (opcion) {
                case 1:
                    addCenter();
                    break;
                case 2:
                    readOneCenter();
                    break;
                case 3:
                    readAllCenters();
                    break;
                case 4:
                    deleteCenter();
                    break;
                case 0:
                    mainMenu();
                default:
                    System.out.println("Elige una opcion entre el 0-4");
                    break;
            }
            opcion = sc.nextInt();
        }
    }

    private void addCenter() {
        System.out.println("Nombre del centro: ");
        String centerName = sc.nextLine();
        System.out.println("Capacidad maxíma de deposito: ");
        int maxStorageCapacity = sc.nextInt();
        try {
            app.createCenter(centerName, maxStorageCapacity);
        } catch (SQLException e) {
            System.err.println("Error creando centro");
        }
    }

    private void readOneCenter() {
        try {
            System.out.println("indica el id del centro del cual quieres la informacion: ");
            int id = sc.nextInt();
            app.displayOneCenter(id);
        } catch (SQLException e) {
            System.err.println("Error al mostrar centro");
        }
    }

    private void readAllCenters() {
        try {
            app.displayAllCenters();
        } catch (SQLException e) {
            System.err.println("Error al mostrar centros");
        }
    }

    private void deleteCenter() {
        System.out.println("Indica el id del centro que quieres eliminar:");
        int idCenter = sc.nextInt();
        try {
            if (app.deleteCenter(idCenter)) {
                System.out.println("Se ha eliminado el centro con id: " + idCenter);
            } else {
                System.out.println("No se ha encontrado ningun centro con id: " + idCenter);
            }
        } catch (SQLException e) {
            System.err.println("Error eliminando centro");
        }
    }
    /*------------------------------FIN------------------------------------*/
    /*--------------------FUNCIONES DE VOLUNTARIOS-------------------------*/

    public void volunteerMenu() {
        System.out.println(
                "1. Añadir voluntarios a un centro\n" +
                        "2. Distribuir voluntarios\n" +
                        "3. Listar voluntarios de un centro\n" +
                        "\n0. Salir"
        );

        int opcion = sc.nextInt();

        while (opcion != 0) {
            switch (opcion) {
                case 1:
                    addVolunteers();
                    break;
                case 2:
                    distributeVolunteers();
                    break;
                case 3:
                    readVolunteerByCenter();
                    break;

                case 0:
                    mainMenu();
                default:
                    System.out.println("Elige una opcion entre el 0-3");
                    break;
            }
            opcion = sc.nextInt();
        }
    }

    private void addVolunteers() {
        System.out.println("Numero de voluntarios: ");
        int volunteerAmount = sc.nextInt();
        System.out.println("ID del centro: ");
        int centerId = sc.nextInt();
        try {
            app.addVolunteers(volunteerAmount, centerId);
        } catch (SQLException e) {
            System.out.println("Error al añadir voluntarios");
        }

    }

    private void distributeVolunteers() {
        System.out.println("Numero de voluntarios que quieres añadir a cada centro: ");
        int volunteerAmount = sc.nextInt();

        try {
            app.distributeVolunteers(volunteerAmount);
        } catch (SQLException e) {
            System.out.println("Error al distribuir los voluntarios");
        }
    }

    private void readVolunteerByCenter() {
        System.out.println("Introduce el id del centro");
        int centerId = sc.nextInt();
        try {
            app.readByCenterId(centerId);
        } catch (SQLException e) {
            System.err.println("Error al mostrar voluntarios");
        }
    }

    /*------------------------------FIN------------------------------------*/
    /*----------------------------MENU START DAY---------------------------*/
    private void collectionMenu() {
        System.out.println(
                "1. Añadir donacion.\n" +
                        "0. Salir."
        );

        int opcion = sc.nextInt();
        while (opcion != 0) {
            switch (opcion) {
                case 1:
                    donation();
                    break;
                case 0:
                    mainMenu();
                default:
                    System.out.println("Elige una opcion entre el 0-4");
                    break;
            }
            opcion = sc.nextInt();
        }
    }

    private void donation() {
        System.out.println("Indica el id del centro: ");
        int centerId = sc.nextInt();
        System.out.println("Indca el id del donante: ");
        int donatorId = sc.nextInt();
        System.out.println("Indica cuanta carne dona: (Si no dona nada pon 0)");
        int carne = sc.nextInt();
        System.out.println("Indica cuanto pescado dona: (Si no dona nada pon 0)");
        int pescado = sc.nextInt();
        System.out.println("Indica cuanta agua dona: (Si no dona nada pon 0)");
        int agua = sc.nextInt();
        System.out.println("Indica cuantas legumbres dona: (Si no dona nada pon 0)");
        int legumbres = sc.nextInt();
        System.out.println("Indica cuanta verdura dona: (Si no dona nada pon 0)");
        int verdura = sc.nextInt();
        System.out.println("Que dia y hora se ha hecho la donacion");
        String hora = sc.nextLine();

        try {
            app.createDonation(centerId, donatorId, carne, pescado, agua, legumbres, verdura, hora);
        } catch (SQLException e) {
            System.err.println("Error al crear donacion.");
        }

        System.out.println("Donacion añadida.");
    }

    /*------------------------------FIN------------------------------------*/
    /*-------------------------MENU ESTADISTICAS---------------------------*/
    public void staticsMenu() {
        System.out.println("1. Comida total.\n" +
                "2. Donaciones totales.\n" +
                "3. Donacion mas grande.\n" +
                "4. Top 5 donaciones.\n" +
                "0. Salir\n");

        int opcion = sc.nextInt();
        while (opcion != 0) {
            switch (opcion) {
                case 1:
                    app.displaytotalFood(); //TODO suma de todos los valores de cada tipo de comida.
                    break;
                case 2:
                    app.totalDonations(); //TODO count de todas las donaciones.
                    break;
                case 3:
                    app.biggestDonation(); //TODO buscar la donacion mas grande.
                    break;
                case 4:
                    app.topDonations(); //TODO buscar las 5 donaciones mas grandes.
                    break;
                case 0:
                    mainMenu();
                default:
                    System.out.println("Elige una opcion entre el 0-4");
                    break;
            }
            opcion = sc.nextInt();
        }
    }
    /*------------------------------FIN------------------------------------*/
}
