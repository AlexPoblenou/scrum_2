package daoInterfaces;

import models.Center;

import java.sql.SQLException;
import java.util.List;

public interface CenterDAO {
    public int create(Center center) throws SQLException;

    public Center readOne(int centerId) throws SQLException;

    public List<Center> readAll() throws SQLException;

    public boolean exists(int centerId) throws SQLException;

    public void update(int centerId, Center center) throws SQLException;

    public void delete(int centerId) throws SQLException;
}
