package daoInterfaces;

import models.Donation;

import java.sql.SQLException;
import java.util.List;

public interface DonationDAO {
    public int create(Donation donation) throws SQLException;

    public Donation readOne(int donationId) throws SQLException;

    public List<Donation> readAll() throws SQLException;
}
