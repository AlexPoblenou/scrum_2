package daoInterfaces;

import models.Volunteer;

import java.sql.SQLException;
import java.util.List;

public interface VolunteerDAO {
    public int create(Volunteer volunteer) throws SQLException;

    public Volunteer readOne(int volunteerId) throws SQLException;

    public List<Volunteer> readAll() throws SQLException;

    public List<Volunteer> readByCenter(int centerId) throws SQLException;

    public void delete(int volunteerId) throws SQLException;

    public void updateCenter(int volunteerId, int centerId) throws SQLException;
}
