package daoInterfaces;

import models.Donator;

import java.sql.SQLException;
import java.util.List;

public interface DonatorDAO {
    public int create(Donator donator) throws SQLException;

    public Donator readOne(int donatorId) throws SQLException;

    public List<Donator> readAll() throws SQLException;

    public boolean exists(int donatorId) throws SQLException;
}
