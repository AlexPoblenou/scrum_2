package daoInterfaces;


import models.Transportation;

import java.sql.SQLException;
import java.util.List;

public interface TransportationDAO {
    public int create(Transportation transportation) throws SQLException;

    public Transportation readLatest(int centerId) throws SQLException;

    public List<Transportation> readAllFrom(String time) throws SQLException;
}
